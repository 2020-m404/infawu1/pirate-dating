﻿namespace Pirate_Dating
{
    // Move class to separate file: CTRL + .
    class Gang
    {
        // Name of pirate gang
        public string name;

        // Main area, in which this gang is active
        public string mainArea;

        // Leader of pirate gang
        public Pirate leader;

        // Multiple pirates are member of a gang.
        // These members are stored in this array of pirates.
        public Pirate[] members;

        // Constructor of class Gang.
        // Every constructor has an access modifier (public), a name which equals to its class name (Gang) 
        // and a list of parameters (empty).
        // A constructor gets called when a new object of a class gets built ("constructed").
        public Gang()
        {
            members = new Pirate[50];
        }

        // Constructor chaining with THIS keyword.
        // The chained constructor is selected based on its parameter list.
        // Chained constructors are executed first.
        public Gang(string nameOfGang) : this()
        {
            name = nameOfGang;
        }
    }
}