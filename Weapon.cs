﻿namespace Pirate_Dating
{
    class Weapon
    {
        // Type of weapon such as musket, pistol, sword, dagger.
        // Correct: Field usually are private.
        private string type;

        // Strength of weapon on scale from 1 to 10 where 10 is strongest
        // This instance variable is a Property with read-write accessors (get/set).
        public float Strength { get; set; }

        // Values for read-only properties can be set in constructor or as default value.
        public string Material { get; } = "Bronze";

        public Weapon()
        {
            Material = "Iron";
        }

        public Weapon(string material)
        {
            Material = material;
        }
    }
}
