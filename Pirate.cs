﻿using System;

namespace Pirate_Dating
{
    // Declare class named Pirate. As per convention, class names start with a capital letter.
    // Within a namespace, names of classes must be unique.
    // Convention: The opening bracket follows on the line after the class name.
    class Pirate
    {
        // Between the opening and closing brackets are MEMBERS of the class declared.

        // This is the declaration of a field which consists of:
        //  - access modifider (public)
        //  - type (string)
        //  - identifier (name)
        public string name;

        // Backing field for storing the pirates age
        private int age;

        // Read-write property for pirates age with self-implemented getter and setter.
        public int Age
        {
            get => age; // short form of: { return age; }
            set
            {
                if (value <= 0)
                {
                    // Throw exception if validation fails
                    throw new ArgumentOutOfRangeException(nameof(Age));
                }

                // assign value of property-assignment to backing field
                age = value;
            }
        }

        // Bei der Implementation des Setters: Schlüsselwort value enthält den Wert der Zuweisung

        // To which gang belongs this pirate
        public Gang gang;

        // On which ship is this pirate sailing
        public Ship sailingOn;

        // What weapons does this pirate have
        public Weapon[] weapons;

        // With which (other) pirate is this pirate in relation (love)
        public Pirate inLoveWith;

        // Read-only property with self-implemented getter
        public bool IsLeader
        {
            get
            {
                if (gang == null)
                {
                    return false;
                }
                return gang.leader == this;
            }
        }


        // Let VS create an empty constructor: ctor [tab] [tab]
        public Pirate()
        {
            weapons = new Weapon[2];
        }
    }
}
