﻿using System;

namespace Pirate_Dating
{
    class Program
    {
        // Starting point of C# program
        // (the common language runtime CLR calls this "main method")
        static void Main(string[] args)
        {
            // Declare a variable of type pirate
            Pirate jack;

            // Assign a value to the declared value.
            // Use the new operator to create a new instance/object of a class.
            jack = new Pirate();

            // Access any member of an object using the member access operator (.)
            jack.name = "Jack";

            // Create an other variable (called emily) and initialize it with another pirate instance.
            Pirate emily = new Pirate();

            // Use the member access operator again to assign a string value ("Emily") as the new pirates value for its name field.
            emily.name = "Emily";

            // Assign the value of the second variable (emily) to the "inRelationWith" field of the first pirate (jack).
            jack.inLoveWith = emily;

            // Create an instance of class Gang...
            Gang piratesOfTheCaribbean = new Gang();
            piratesOfTheCaribbean.name = "Pirates of the Caribbean";

            // ... and assign at least one Pirate object to the gangs "member" field.
            /*
             * Following code won't work, since we haven't initialized the array "members":
             * 
             * gang.members[0] = jack;
             * gang.members[1] = emily;
             * 
             * Initialization of members field happens in the Gang class' constructor:
             * gang.members = new Pirate[50];
             */

            piratesOfTheCaribbean.members[0] = jack;
            piratesOfTheCaribbean.members[1] = emily;

            // Create another Gang instance
            Gang freibeuter = new Gang("Freibeuter");
            freibeuter.members[0] = emily;
            freibeuter.members[1] = jack;

            Console.WriteLine($"Name der Gang: {freibeuter.name}");

            // Create new ship instance with default constructor
            Ship titanic = new Ship();



            // Create new Gang object with object initializer
            Gang gangA = new Gang { }; // identical to: new Gang();

            // Order and count of members does not matter.
            // Allowed are public instance variables.
            Gang gangB = new Gang
            {
                name = "Gang B",
                leader = jack
            };

            // Object initializer are "executed" after the constructors execution.
            Gang gangC = new Gang("Gang C") { name = "Gang c" };
            bool nameOfObjectInitializer = gangC.name == "Gang c"; // true;




            // Write access to Strength property
            Weapon excalibur = new Weapon { Strength = 8.2f };

            // Read access to Strength property
            Console.WriteLine($"Strength of excalibur: {excalibur.Strength}");

            // Values for Read-only properties can be set in constructor, but not in object initializers.
            Weapon pistol = new Weapon("Iron");

            /*
             * It is not possible to set a value for a read-only property:
             * 
             * pistol.Material = "Wood"; // compile-time error
             */

            // Access custom getter on Pirate instance
            bool jackIsLeader = jack.IsLeader;
            Console.WriteLine($"Jack is leader: {jackIsLeader}");

            jack.Age = -5;
            Console.WriteLine($"Alter von Jack: {jack.Age}");

        }
    }
}